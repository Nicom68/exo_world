Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.



```sql
SELECT  country, COUNT(distinct c.customerNumber) AS client_nber, language AS lang, SUM(quantityOrdered*priceEach) AS TotalOrder
    -> FROM customers AS c
    -> JOIN orders AS o ON c.customerNumber = o.customerNumber
    -> JOIN orderdetails AS ords ON o.orderNumber = ords.orderNumber
    -> GROUP BY country, lang;



+---------+-------------+-----------+------------+
| country | client_nber | lang      | TotalOrder |
+---------+-------------+-----------+------------+
| AUS     |           5 | English   |  562582.59 |
| AUT     |           2 | German    |  188540.06 |
| BEL     |           2 | Dutch     |  100068.76 |
| CAN     |           3 | English   |  205911.86 |
| CHE     |           1 | German    |  108777.92 |
| DEU     |           3 | German    |  196470.99 |
| DNK     |           2 | Danish    |  218994.92 |
| ESP     |           5 | Spanish   | 1099389.09 |
| FIN     |           3 | Finnish   |  295149.35 |
| FRA     |          12 | French    | 1007374.02 |
| GBR     |           5 | English   |  436947.44 |
| HKG     |           1 | English   |   45480.79 |
| IRL     |           1 | English   |   49898.27 |
| ITA     |           4 | Italian   |  360616.81 |
| JPN     |           2 | Japanese  |  167909.95 |
| NOR     |           3 | Norwegian |  270846.30 |
| NZL     |           4 | English   |  476847.01 |
| PHL     |           1 | Pilipino  |   87468.30 |
| SGP     |           2 | Chinese   |  263997.78 |
| SWE     |           2 | Swedish   |  187638.35 |
| USA     |          35 | English   | 3273280.05 |
+---------+-------------+-----------+------------+
21 rows in set (0,01 sec)
```

Question 2 : La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.

```sql
SELECT customerName, SUM(quantityOrdered*priceEach) AS TotalOrder
FROM customers AS c
JOIN orders AS o ON c.customerNumber = o.customerNumber
JOIN orderdetails AS ords ON o.orderNumber = ords.orderNumber
GROUP BY customerName 
ORDER BY TotalOrder DESC
LIMIT 10;

+------------------------------+------------+
| customerName                 | TotalOrder |
+------------------------------+------------+
| Euro+ Shopping Channel       |  820689.54 |
| Mini Gifts Distributors Ltd. |  591827.34 |
| Australian Collectors, Co.   |  180585.07 |
| Muscle Machine Inc           |  177913.95 |
| La Rochelle Gifts            |  158573.12 |
| Dragon Souveniers, Ltd.      |  156251.03 |
| Down Under Souveniers, Inc   |  154622.08 |
| Land of Toys Inc.            |  149085.15 |
| AV Stores, Co.               |  148410.09 |
| The Sharp Gifts Warehouse    |  143536.27 |
+------------------------------+------------+
10 rows in set (0,01 sec)
```


Question 3 - La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).


```sql

SELECT AVG(DATEDIFF(shippedDate,orderDate)) AS AverageTimeDiff FROM orders;

+-----------------+
| AverageTimeDiff |
+-----------------+
|          3.7564 |
+-----------------+
1 row in set (0,00 sec)
```

Question 4 - Les 10 produits les plus vendus.

``` sql
SELECT p.productCode, p.productName, SUM(quantityOrdered) AS Best_Sales
FROM products AS p
JOIN orderdetails AS ord ON p.productCode = ord.productCode
JOIN orders AS o ON ord.orderNumber = o.orderNumber
GROUP BY p.productCode, p.productName
ORDER BY Best_Sales DESC
LIMIT 10;

| productCode | productName                             | Best_Sales |
+-------------+-----------------------------------------+------------+
| S18_3232    | 1992 Ferrari 360 Spider red             |       1808 |
| S18_1342    | 1937 Lincoln Berline                    |       1111 |
| S700_4002   | American Airlines: MD-11S               |       1085 |
| S18_3856    | 1941 Chevrolet Special Deluxe Cabriolet |       1076 |
| S50_1341    | 1930 Buick Marquette Phaeton            |       1074 |
| S18_4600    | 1940s Ford truck                        |       1061 |
| S10_1678    | 1969 Harley Davidson Ultimate Chopper   |       1057 |
| S12_4473    | 1957 Chevy Pickup                       |       1056 |
| S18_2319    | 1964 Mercedes Tour Bus                  |       1053 |
| S24_3856    | 1956 Porsche 356A Coupe                 |       1052 |
+-------------+-----------------------------------------+------------+
10 rows in set (0,01 sec)
```

Question 5 - Pour chaque pays le produits le plus vendu.


```sql
WITH RankedProducts AS (
    SELECT
        p.productCode,
        p.productName,
        SUM(quantityOrdered) AS Best_Sales,
        c.country,
        RANK() OVER (PARTITION BY c.country ORDER BY SUM(quantityOrdered) DESC) AS SalesRank
    FROM products AS p
    JOIN orderdetails AS ord ON p.productCode = ord.productCode
    JOIN orders AS o ON ord.orderNumber = o.orderNumber
    JOIN customers AS c ON o.customerNumber = c.customerNumber
    GROUP BY p.productCode, p.productName, c.country
)
SELECT
    productCode,
    productName,
    Best_Sales,
    country
FROM
    RankedProducts
WHERE
    SalesRank = 1
ORDER BY country;


+---------------------------------------------+------------+---------+
| productCode | productName                                 | Best_Sales | country |
+-------------+---------------------------------------------+------------+---------+
| S18_2949    | 1913 Ford Model T Speedster                 |        231 | AUS     |
| S12_4675    | 1969 Dodge Charger                          |        128 | AUT     |
| S18_3856    | 1941 Chevrolet Special Deluxe Cabriolet     |         95 | BEL     |
| S32_3522    | 1996 Peterbilt 379 Stake Bed with Outrigger |         97 | CAN     |
| S700_2824   | 1982 Camaro Z28                             |         97 | CAN     |
| S18_1889    | 1948 Porsche 356-A Roadster                 |         91 | CHE     |
| S24_3371    | 1971 Alpine Renault 1600s                   |        117 | DEU     |
| S18_3685    | 1948 Porsche Type 356 Roadster              |        101 | DNK     |
| S18_3232    | 1992 Ferrari 360 Spider red                 |        336 | ESP     |
| S18_3232    | 1992 Ferrari 360 Spider red                 |        141 | FIN     |
| S50_4713    | 2002 Yamaha YZR M1                          |        278 | FRA     |
| S24_2887    | 1952 Citroen-15CV                           |        162 | GBR     |
| S24_1785    | 1928 British Royal Navy Airplane            |         79 | HKG     |
| S18_4027    | 1970 Triumph Spitfire                       |         50 | IRL     |
| S24_1785    | 1928 British Royal Navy Airplane            |        162 | ITA     |
| S700_4002   | American Airlines: MD-11S                   |         92 | JPN     |
| S10_1678    | 1969 Harley Davidson Ultimate Chopper       |         89 | NOR     |
| S18_3278    | 1969 Dodge Super Bee                        |        142 | NZL     |
| S18_4721    | 1957 Corvette Convertible                   |         94 | PHL     |
| S18_4600    | 1940s Ford truck                            |        128 | SGP     |
| S18_4600    | 1940s Ford truck                            |         97 | SWE     |
| S12_4473    | 1957 Chevy Pickup                           |        523 | USA     |
+-------------+---------------------------------------------+------------+---------+
22 rows in set (0,02 sec)

```