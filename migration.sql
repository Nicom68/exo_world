use classicmodels;

-- Modification du modèle.
-- Utiliser `classicmodels.customers.country` pour stocker la clef 
-- du pays venant de la table 'world.country'

-- UPDATE customers
-- SET salesRepEmployeeNumber = employeeNumber
-- WHERE salesRepEmployeeNumber IS NOT NULL;

-- Insertion des valeurs dans la table "productlines" de la BDD v2

INSERT INTO v2.productlines
SELECT *
FROM classicmodels.productlines;

-- Insertion des valeurs dans la table "products" de la BDD v2

INSERT INTO v2.products
SELECT * 
FROM classicmodels.products;

-- Normalisation des pays
UPDATE classicmodels.offices SET country = "United Kingdom" WHERE country = "UK";
UPDATE classicmodels.offices SET country = "United States" WHERE country = "USA";


-- -- sélectionner les champs dans la table "classicmodels.offices" et "word.country"
-- -- Insérer les valeurs dans v2.offices


INSERT INTO v2.offices(
    officeCode,
    city,
    phone,
    addressLine1,
    addressLine2,
    state,
    country,
    postalCode,
    territory
    ) 

SELECT 
    officeCode,
    city,
    phone,
    addressLine1,
    addressLine2,
    state,
    world.country.Code,
    postalCode,
    territory 
FROM classicmodels.offices
JOIN world.country ON classicmodels.offices.country = world.country.Name;


-- -- -- sélectionner les champs dans la table "customers"
-- -- -- Insérer les données de "classicmodels" et "word" dans la table v2
-- -- -- Insérer les valeurs dans la table "Employee" d'abord, on aura besoin pour le champ  salesRepEmployeeNumber,



INSERT INTO v2.employees
SELECT
    employeeNumber,
    lastName,
    firstName,
    extension, 
    email,
    officeCode, 
    reportsTo,
    jobTitle
FROM classicmodels.employees;

-- création d'une table temporaire
-- sélectionner les champs dans la table "classicmodels.customers" et "word.country"

create table tmp (
  customerNumber int,
  customerName varchar(50) NOT NULL,
  contactLastName varchar(50) NOT NULL,
  contactFirstName varchar(50) NOT NULL,
  phone varchar(50) NOT NULL,
  addressLine1 varchar(50) NOT NULL,
  addressLine2 varchar(50) DEFAULT NULL,
  city varchar(50) NOT NULL,
  state varchar(50) DEFAULT NULL,
  postalCode varchar(15) DEFAULT NULL,
  country varchar(50) NOT NULL,
  salesRepEmployeeNumber int DEFAULT NULL,
  creditLimit decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (customerNumber),
  FOREIGN KEY (salesRepEmployeeNumber) REFERENCES employees (employeeNumber)
);

insert into tmp table classicmodels.customers;
UPDATE tmp SET country = "United Kingdom" WHERE country = "UK";
UPDATE tmp SET country = "United States" WHERE country = "USA";
UPDATE tmp SET country = "Russian Federation" WHERE country = "Russia";

/* Update Norwway which appears duplicate with and without spaces when:
select country, count(country) from customers group by country order by country;*/
UPDATE tmp SET country = "Norway" WHERE country = "Norway  ";

INSERT INTO v2.customers (
    customerNumber,
    customerName,
    contactLastName,
    contactFirstName,
    phone,
    addressLine1,
    addressLine2,
    city,
    state,
    postalCode,
    country,
    salesRepEmployeeNumber,
    creditLimit
)
SELECT
    customerNumber,
    customerName,
    contactLastName,
    contactFirstName,
    phone,
    addressLine1,
    addressLine2,
    city,
    state,
    postalCode,
    world.country.code,
    salesRepEmployeeNumber,
    creditLimit
FROM tmp
JOIN world.country ON tmp.country = world.country.Name;
drop table tmp;


-- Remplacer le type du champ `classicmodel.orders.status` par un `enum` de tous les états différents dans la base de données.
-- Sélectionner les champs dans la table "Orders"

INSERT INTO v2.orders (
  orderNumber,
  orderDate,
  requiredDate,
  shippedDate,
  status,
  comments,
  customerNumber
)
SELECT
  orderNumber,
  orderDate,
  requiredDate,
  shippedDate,
    CASE
    WHEN status = 'Shipped' THEN 'ENUM_SHIPPED'
    WHEN status = 'In Process' THEN 'ENUM_IN_PROCESS'
    WHEN status = 'Disputed' THEN 'ENUM_DISPUTED'
    WHEN status = 'Resolved' THEN 'ENUM_RESOLVED'
    WHEN status = 'On Hold' THEN 'ENUM_ON_HOLD'
    WHEN status = 'Cancelled' THEN 'ENUM_CANCELLED'
    ELSE 'UNKNOWN'
  END AS status,
  comments,
  customerNumber
FROM classicmodels.orders;

-- Ajouter un champ `classicmodel.customer.language` qui devra contenir le code de la langue avec laquelle s’adresser au client en ISO 639-1.

-- Ajouter un champ "language" à la table "customers"
ALTER TABLE v2.customers
ADD COLUMN Language varchar(55);

-- Récupérer la langue la plus parlée par pays à partir de la table "countrylanguage"


select t.code as CountryCode, t.language as language 
from(
    select c.code, 
        cl.language, 
        cl.percentage,
        ROW_NUMBER() OVER (PARTITION BY c.code ORDER BY cl.percentage DESC) AS rn
    from world.country as c 
    join world.countrylanguage as cl on cl.countrycode = c.code 
    where cl.isOfficial = 'T'
) as t
where t.rn = 1;

  -- Insérer les langues les plus parlées par pays. A chaque pays doit correspondre 
  -- la langue la plus parlée dans la table "customers" de v2

UPDATE v2.customers
JOIN (
    SELECT t.code AS CountryCode, t.language AS NewLanguage
    FROM (
        SELECT c.code, 
            cl.language, 
            cl.percentage,
            ROW_NUMBER() OVER (PARTITION BY c.code ORDER BY cl.percentage DESC) AS rn
        FROM world.country AS c 
        JOIN world.countrylanguage AS cl ON cl.countrycode = c.code 
        WHERE cl.isOfficial = 'T'
    ) AS t
    WHERE t.rn = 1
) AS subquery ON customers.country = subquery.CountryCode
SET customers.language = subquery.NewLanguage;

-- Table orderdetails

INSERT INTO v2.orderdetails (
  orderNumber,
  productCode,
  quantityOrdered, 
  priceEach, 
  orderLineNumber
)
SELECT 
  orderNumber,
  productCode,
  quantityOrdered, 
  priceEach, 
  orderLineNumber
FROM classicmodels.orderdetails;