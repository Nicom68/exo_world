# QUESTION 1 : Le modèle physique des données


La base de données "world" comporte 3 tables :
- city
- country
- coutrylanguage

## TABLE city (
    ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Name char(35) NOT NULL,
    CountryCode char(3) NOT NULL, 
    District char(20) NOT NULL,
    Population int NOT NULL
);

## TABLE country (
    Code char(3) NOT NULL PRIMARY KEY,
    Name char(52) NOT NULL,
    Continent enum ('Asia', 'Europe','North America','Africa', 'Oceania', 'Antartica', 'South America'),
    Region char(26) NOT NULL,
    SurfaceArea decimal (10,2) NOT NULL,
    IndepYear smallint,
    Population int NOT NULL,
    LifeExpantancy decimal(10,2),
    GNP decimal (10,2),
    GNPOld decimal (10,2),
    LocalName char(45),
    GovernmentForm char(45) NOT NULL,
    HeadOfState char(60),
    Capital int,
    Code2 char(2) NOT NULL
);

## TABLE countrylanguage (
    CountryCode char(3) NOT NULL PRIMARY KEY,
    Language char(30) NOT NULL PRIMARY KEY,
    IsOfficial enum('T','F') NOT NULL,
    Percentage decimal(4,1) NOT NULL
);

# QUESTION 2 : pour chaque type utilisé pour au moins un attribut : le definir et expliquer pourquoi on a choisit ce type ?

## Table city

ID : int.
on choisit int (integer), car l'id sera un nombre entier. Il est défini en auto_increment. Pour chaque nouvelle entrée, l'id sera incrémenté de + 1. La valeur de l'id ne peut pas être nulle,on ajoute le paramètre NOT NULL

Name : nom de la ville.char(35) permet d'écrire jusqu'à 35 caractères.

CountryCode : char(3) NOT NULL.
CountryCode correspond au code du pays. char(3) pour 3 caractères respecte la norme ISO 3166-1 alpha-3 sur l'écriture des code pays.

District : char(20) NOT NULL.
District correspond à la région, province ou l'état selon les pays.

Population : int.

## TABLE country 

Code : char(3) NOT NULL PRIMARY KEY
Code correspond au code du pays comme CountryCode, char(3) pour 3 caractères respecte la norme ISO 3166-1 alpha-3 sur l'écriture des code pays.

Name : char(52) NOT NULL
    Continent enum ('Asia', 'Europe','North America','Africa', 'Oceania', 'Antartica', 'South America'),
Name correspond au nom du continent. La liste des continents sera donnée sous forme de liste. Le paramètre Enum permet d'avoir la liste.

Region : char(26) NOT NULL
Region correspond à des parties du monde (Southern Europe, Carribean, etc....)

SurfaceArea: decimal (10,2) NOT NULL,
Correspond à la surface du pays. Le nombre sera en décimal. (10,2)
signifie un nombre maximum de 10 chiffres avec 2 décimales après la virgule. L'unité est en km².

IndepYear :  smallint,
Date d'indépendance du pays.

Population :  int NOT NULL

LifeExpantancy :  decimal(10,2),
Espérance de vie, decimal(10,2) correspond à un nombre décimal à 10 chiffres avec 2 décimales après la virgule.

GNP :  decimal (10,2)
Gross National Product = produit national brut exprimé en dollar US
exprimé en millions.
    
GNPOld :  decimal (10,2),
Gross National Product de l'année précedente

LocalName char(45),
nom du pays en langue locale

GovernmentForm :  char(45) NOT NULL,
Type de gouvernement par pays.


HeadOfState :  char(60),
Chef d'état
    
Capital : int
Capitale du pays donné par un identifiant

    
Code2 char(2) NOT NULL
Conversion du code pays en 2 caractères.

## TABLE countrylanguage 

CountryCode : char(3) NOT NULL PRIMARY KEY,
vu précedemment.

Language : char(30) NOT NULL PRIMARY KEY,
langage du pays

IsOfficial  : enum('T','F') NOT NULL,
Vérifie si le langage est officiel ou non

Percentage : decimal(4,1) NOT NULL

# Question 3 : une liste, pour chaque table, des contraintes (pourquoi il y a ces contraintes ?)


TABLE city (
    ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Name char(35) NOT NULL,
    CountryCode char(3) NOT NULL, 
    District char(20) NOT NULL,
    Population int NOT NULL
);

# Question 4 : les requêtes

## Question 1 - Liste des type de gouvernement avec nombre de pays pour chaque.
```sql
SELECT GovernmentForm AS type_de_gouvernement, COUNT(Code) AS nombre_de_pays FROM country 
GROUP BY GovernmentForm; 

----------------------------------------------+----------------+
| type_de_gouvernement                         | nombre_de_pays |
+----------------------------------------------+----------------+
| Nonmetropolitan Territory of The Netherlands |              2 |
| Islamic Emirate                              |              1 |
| Republic                                     |            123 |
| Dependent Territory of the UK                |             12 |
| Parliamentary Coprincipality                 |              1 |
| Emirate Federation                           |              1 |
| Federal Republic                             |             14 |
| US Territory                                 |              3 |
| Co-administrated                             |              1 |
| Nonmetropolitan Territory of France          |              4 |
| Constitutional Monarchy                      |             29 |
| Constitutional Monarchy, Federation          |              4 |
| Monarchy (Emirate)                           |              1 |
| Monarchy (Sultanate)                         |              2 |
| Monarchy                                     |              5 |
| Dependent Territory of Norway                |              2 |
| Territory of Australia                       |              4 |
| Federation                                   |              1 |
| People'sRepublic                             |              1 |
| Nonmetropolitan Territory of New Zealand     |              3 |
| Socialistic Republic                         |              3 |
| Occupied by Marocco                          |              1 |
| Part of Denmark                              |              2 |
| Overseas Department of France                |              4 |
| Special Administrative Region of China       |              2 |
| Islamic Republic                             |              2 |
| Constitutional Monarchy (Emirate)            |              1 |
| Socialistic State                            |              1 |
| Commonwealth of the US                       |              2 |
| Territorial Collectivity of France           |              2 |
| Autonomous Area                              |              1 |
| Administrated by the UN                      |              1 |
| Dependent Territory of the US                |              1 |
| Independent Church State                     |              1 |
| Parlementary Monarchy                        |              1 |
+----------------------------------------------+----------------

```

## Question 2 - Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?
```sql
SELECT DISTINCT Language
FROM countrylanguage;```

Réponse : pas d'enum car existe 458 langages différents.

```sql
+---------------------------+
| Language                  |
+---------------------------+
| Dutch                     |
| English                   |
| Papiamento                |
| Spanish                   |
| Balochi                   |
| Dari                      |
| Pashto                    |
| Turkmenian                |
| Uzbek                     |
| Ambo                      |
| Chokwe                    |
| Kongo                     |
| Luchazi                   |
| Luimbe-nganguela          |
| Luvale                    |
| Mbundu                    |
| Nyaneka-nkhumbi           |
| Ovimbundu                 |
| Albaniana                 |
| Greek                     |
| Macedonian                |
| Catalan                   |
| French                    |
| Portuguese                |
| Arabic                    |
| Hindi                     |
| Indian Languages          |
| Italian                   |
| Armenian                  |
| Azerbaijani               |
| Samoan                    |
| Tongan                    |
| Creole English            |
| Canton Chinese            |
| German                    |
| Serbo-Croatian            |
| Vietnamese                |
| Czech                     |
| Hungarian                 |
| Polish                    |
| Romanian                  |
| Slovene                   |
| Turkish                   |
| Lezgian                   |
| Russian                   |
| Kirundi                   |
| Swahili                   |
| Adja                      |
| Aizo                      |
| Bariba                    |
| Fon                       |
| Ful                       |
| Joruba                    |
| Somba                     |
| Busansi                   |
| Dagara                    |
| Dyula                     |
| Gurma                     |
| Mossi                     |
| Bengali                   |
| Chakma                    |
| Garo                      |
| Khasi                     |
| Marma                     |
| Santhali                  |
| Tripuri                   |
| Bulgariana                |
| Romani                    |
| Creole French             |
| Belorussian               |
| Ukrainian                 |
| Garifuna                  |
| Maya Languages            |
| Aimará                    |
| Guaraní                   |
| Ketšua                    |
| Japanese                  |
| Bajan                     |
| Chinese                   |
| Malay                     |
| Malay-English             |
| Asami                     |
| Dzongkha                  |
| Nepali                    |
| Khoekhoe                  |
| Ndebele                   |
| San                       |
| Shona                     |
| Tswana                    |
| Banda                     |
| Gbaya                     |
| Mandjia                   |
| Mbum                      |
| Ngbaka                    |
| Sara                      |
| Eskimo Languages          |
| Punjabi                   |
| Romansh                   |
| Araucan                   |
| Rapa nui                  |
| Dong                      |
| Hui                       |
| Mantšu                    |
| Miao                      |
| Mongolian                 |
| Puyi                      |
| Tibetan                   |
| Tujia                     |
| Uighur                    |
| Yi                        |
| Zhuang                    |
| [South]Mande              |
| Akan                      |
| Gur                       |
| Kru                       |
| Malinke                   |
| Bamileke-bamum            |
| Duala                     |
| Fang                      |
| Maka                      |
| Mandara                   |
| Masana                    |
| Tikar                     |
| Boa                       |
| Luba                      |
| Mongo                     |
| Ngala and Bangi           |
| Rundi                     |
| Rwanda                    |
| Teke                      |
| Zande                     |
| Mbete                     |
| Mboshi                    |
| Punu                      |
| Sango                     |
| Maori                     |
| Arawakan                  |
| Caribbean                 |
| Chibcha                   |
| Comorian                  |
| Comorian-Arabic           |
| Comorian-French           |
| Comorian-madagassi        |
| Comorian-Swahili          |
| Crioulo                   |
| Moravian                  |
| Silesiana                 |
| Slovak                    |
| Southern Slavic Languages |
| Afar                      |
| Somali                    |
| Danish                    |
| Norwegian                 |
| Swedish                   |
| Berberi                   |
| Sinaberberi               |
| Bilin                     |
| Hadareb                   |
| Saho                      |
| Tigre                     |
| Tigrinja                  |
| Basque                    |
| Galecian                  |
| Estonian                  |
| Finnish                   |
| Amhara                    |
| Gurage                    |
| Oromo                     |
| Sidamo                    |
| Walaita                   |
| Saame                     |
| Fijian                    |
| Faroese                   |
| Kosrean                   |
| Mortlock                  |
| Pohnpei                   |
| Trukese                   |
| Wolea                     |
| Yap                       |
| Mpongwe                   |
| Punu-sira-nzebi           |
| Gaeli                     |
| Kymri                     |
| Abhyasi                   |
| Georgiana                 |
| Osseetti                  |
| Ewe                       |
| Ga-adangme                |
| Kissi                     |
| Kpelle                    |
| Loma                      |
| Susu                      |
| Yalunka                   |
| Diola                     |
| Soninke                   |
| Wolof                     |
| Balante                   |
| Mandyako                  |
| Bubi                      |
| Greenlandic               |
| Cakchiquel                |
| Kekchí                    |
| Mam                       |
| Quiché                    |
| Chamorro                  |
| Korean                    |
| Philippene Languages      |
| Chiu chau                 |
| Fukien                    |
| Hakka                     |
| Miskito                   |
| Haiti Creole              |
| Bali                      |
| Banja                     |
| Batakki                   |
| Bugi                      |
| Javanese                  |
| Madura                    |
| Minangkabau               |
| Sunda                     |
| Gujarati                  |
| Kannada                   |
| Malajalam                 |
| Marathi                   |
| Orija                     |
| Tamil                     |
| Telugu                    |
| Urdu                      |
| Irish                     |
| Bakhtyari                 |
| Gilaki                    |
| Kurdish                   |
| Luri                      |
| Mazandarani               |
| Persian                   |
| Assyrian                  |
| Icelandic                 |
| Hebrew                    |
| Friuli                    |
| Sardinian                 |
| Circassian                |
| Ainu                      |
| Kazakh                    |
| Tatar                     |
| Gusii                     |
| Kalenjin                  |
| Kamba                     |
| Kikuyu                    |
| Luhya                     |
| Luo                       |
| Masai                     |
| Meru                      |
| Nyika                     |
| Turkana                   |
| Kirgiz                    |
| Tadzhik                   |
| Khmer                     |
| Tšam                      |
| Kiribati                  |
| Tuvalu                    |
| Lao                       |
| Lao-Soung                 |
| Mon-khmer                 |
| Thai                      |
| Bassa                     |
| Gio                       |
| Grebo                     |
| Mano                      |
| Mixed Languages           |
| Singali                   |
| Sotho                     |
| Zulu                      |
| Lithuanian                |
| Luxembourgish             |
| Latvian                   |
| Mandarin Chinese          |
| Monegasque                |
| Gagauzi                   |
| Malagasy                  |
| Dhivehi                   |
| Mixtec                    |
| Náhuatl                   |
| Otomí                     |
| Yucatec                   |
| Zapotec                   |
| Marshallese               |
| Bambara                   |
| Senufo and Minianka       |
| Songhai                   |
| Tamashek                  |
| Maltese                   |
| Burmese                   |
| Chin                      |
| Kachin                    |
| Karen                     |
| Kayah                     |
| Mon                       |
| Rakhine                   |
| Shan                      |
| Bajad                     |
| Buryat                    |
| Dariganga                 |
| Dorbet                    |
| Carolinian                |
| Chuabo                    |
| Lomwe                     |
| Makua                     |
| Marendje                  |
| Nyanja                    |
| Ronga                     |
| Sena                      |
| Tsonga                    |
| Tswa                      |
| Hassaniya                 |
| Tukulor                   |
| Zenaga                    |
| Bhojpuri                  |
| Chichewa                  |
| Ngoni                     |
| Yao                       |
| Dusun                     |
| Iban                      |
| Mahoré                    |
| Afrikaans                 |
| Caprivi                   |
| Herero                    |
| Kavango                   |
| Nama                      |
| Ovambo                    |
| Malenasian Languages      |
| Polynesian Languages      |
| Hausa                     |
| Kanuri                    |
| Songhai-zerma             |
| Bura                      |
| Edo                       |
| Ibibio                    |
| Ibo                       |
| Ijo                       |
| Tiv                       |
| Sumo                      |
| Niue                      |
| Fries                     |
| Maithili                  |
| Newari                    |
| Tamang                    |
| Tharu                     |
| Nauru                     |
| Brahui                    |
| Hindko                    |
| Saraiki                   |
| Sindhi                    |
| Cuna                      |
| Embera                    |
| Guaymí                    |
| Pitcairnese               |
| Bicol                     |
| Cebuano                   |
| Hiligaynon                |
| Ilocano                   |
| Maguindanao               |
| Maranao                   |
| Pampango                  |
| Pangasinan                |
| Pilipino                  |
| Waray-waray               |
| Palau                     |
| Papuan Languages          |
| Tahitian                  |
| Avarian                   |
| Bashkir                   |
| Chechen                   |
| Chuvash                   |
| Mari                      |
| Mordva                    |
| Udmur                     |
| Bari                      |
| Beja                      |
| Chilluk                   |
| Dinka                     |
| Fur                       |
| Lotuko                    |
| Nubian Languages          |
| Nuer                      |
| Serer                     |
| Bullom-sherbro            |
| Kono-vai                  |
| Kuranko                   |
| Limba                     |
| Mende                     |
| Temne                     |
| Nahua                     |
| Sranantonga               |
| Czech and Moravian        |
| Ukrainian and Russian     |
| Swazi                     |
| Seselwa                   |
| Gorane                    |
| Hadjarai                  |
| Kanem-bornu               |
| Mayo-kebbi                |
| Ouaddai                   |
| Tandjile                  |
| Ane                       |
| Kabyé                     |
| Kotokoli                  |
| Moba                      |
| Naudemba                  |
| Watyi                     |
| Kuy                       |
| Tokelau                   |
| Arabic-French             |
| Arabic-French-English     |
| Ami                       |
| Atayal                    |
| Min                       |
| Paiwan                    |
| Chaga and Pare            |
| Gogo                      |
| Ha                        |
| Haya                      |
| Hehet                     |
| Luguru                    |
| Makonde                   |
| Nyakusa                   |
| Nyamwesi                  |
| Shambala                  |
| Acholi                    |
| Ganda                     |
| Gisu                      |
| Kiga                      |
| Lango                     |
| Lugbara                   |
| Nkole                     |
| Soga                      |
| Teso                      |
| Tagalog                   |
| Karakalpak                |
| Goajiro                   |
| Warrau                    |
| Man                       |
| Muong                     |
| Nung                      |
| Tho                       |
| Bislama                   |
| Futuna                    |
| Wallis                    |
| Samoan-English            |
| Soqutri                   |
| Northsotho                |
| Southsotho                |
| Venda                     |
| Xhosa                     |
| Bemba                     |
| Chewa                     |
| Lozi                      |
| Nsenga                    |
| Tonga                     |
+---------------------------+
458 rows in set (0,01 sec)'''

## Question 3 - D’apres la BDD, combien de personne dans le monde parle anglais ?

```sql
SELECT SUM((Percentage/100)*Population) AS TotalEnglishPopulation
FROM countrylanguage
JOIN country ON countrylanguage.CountryCode = country.code
WHERE Language ='English';

+------------------------+
| TotalEnglishPopulation |
+------------------------+
|        347077867.30000 |
+------------------------+
1 row in set (0,01 sec)```

## Question 4 - Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.

```sql
SELECT Language, SUM((Percentage/100)*Population) AS TotalPopulation
FROM countrylanguage
JOIN country ON countrylanguage.CountryCode = country.code
GROUP BY Language
ORDER BY TotalPopulation DESC;

+---------------------------+------------------+
| Language                  | TotalPopulation  |
+---------------------------+------------------+
| Chinese                   | 1191843539.00000 |
| Hindi                     |  405633070.00000 |
| Spanish                   |  355029462.00000 |
| English                   |  347077867.30000 |
| Arabic                    |  233839238.70000 |
| Bengali                   |  209304719.00000 |
| Portuguese                |  177595269.40000 |
| Russian                   |  160807561.30000 |
| Japanese                  |  126814108.00000 |
| Punjabi                   |  104025371.00000 |
| German                    |   92133584.70000 |
| Javanese                  |   83570158.00000 |
| Telugu                    |   79065636.00000 |
| Marathi                   |   75019094.00000 |
| Korean                    |   72291372.00000 |
| Vietnamese                |   70616218.00000 |
| French                    |   69980880.40000 |
| Tamil                     |   68691536.00000 |
| Urdu                      |   63589470.00000 |
| Turkish                   |   62205657.20000 |
| Italian                   |   59864483.20000 |
| Gujarati                  |   48655776.00000 |
| Malay                     |   41517994.00000 |
| Kannada                   |   39532818.00000 |
| Polish                    |   39525035.10000 |
| Ukrainian                 |   36593408.00000 |
| Malajalam                 |   36491832.00000 |
| Thai                      |   33996960.00000 |
| Sunda                     |   33512906.00000 |
| Orija                     |   33450846.00000 |
| Pashto                    |   32404553.00000 |
| Burmese                   |   31471590.00000 |
| Persian                   |   31124734.00000 |
| Hausa                     |   29225396.00000 |
| Joruba                    |   24868874.00000 |
| Ful                       |   23704612.00000 |
| Romanian                  |   23457777.30000 |
| Uzbek                     |   22535760.00000 |
| Pilipino                  |   22258331.00000 |
| Dutch                     |   21388666.00000 |
| Ibo                       |   20182586.00000 |
| Lao                       |   20167307.00000 |
| Oromo                     |   19395150.00000 |
| Kurdish                   |   19062628.00000 |
| Azerbaijani               |   19014911.00000 |
| Amhara                    |   18769500.00000 |
| Sindhi                    |   18464994.00000 |
| Zhuang                    |   17885812.00000 |
| Cebuano                   |   17700311.00000 |
| Serbo-Croatian            |   16762504.70000 |
| Malagasy                  |   15800413.00000 |
| Asami                     |   15527778.00000 |
| Saraiki                   |   15335334.00000 |
| Akan                      |   15026888.00000 |
| Min                       |   14844752.00000 |
| Berberi                   |   13817820.00000 |
| Rwanda                    |   13750258.00000 |
| Nepali                    |   12799872.00000 |
| Somali                    |   12770598.00000 |
| Hungarian                 |   12652201.90000 |
| Khmer                     |   11810683.00000 |
| Greek                     |   11638803.60000 |
| Mantšu                    |   11498022.00000 |
| Kongo                     |   11480181.00000 |
| Singali                   |   11352681.00000 |
| Hui                       |   10220464.00000 |
| Shona                     |    9892055.00000 |
| Miao                      |    9661394.00000 |
| Zulu                      |    9508689.00000 |
| Luba                      |    9297720.00000 |
| Kazakh                    |    9258230.00000 |
| Mossi                     |    9185870.00000 |
| Madura                    |    9120601.00000 |
| Czech                     |    8362000.80000 |
| Swedish                   |    8255142.60000 |
| Haiti Creole              |    8222000.00000 |
| Belorussian               |    7671574.00000 |
| Uighur                    |    7665348.00000 |
| Yi                        |    7665348.00000 |
| Dari                      |    7293120.00000 |
| Mongolian                 |    7207888.00000 |
| Xhosa                     |    7146729.00000 |
| Nyamwesi                  |    7072087.00000 |
| Ilocano                   |    7064931.00000 |
| Bulgariana                |    7036276.80000 |
| Mongo                     |    6973290.00000 |
| Hiligaynon                |    6912997.00000 |
| Catalan                   |    6690841.30000 |
| Canton Chinese            |    6628268.00000 |
| Kirundi                   |    6567795.00000 |
| Balochi                   |    6456116.00000 |
| Tigrinja                  |    6395030.00000 |
| Tujia                     |    6387790.00000 |
| Chichewa                  |    6369275.00000 |
| Kikuyu                    |    6286720.00000 |
| Ibibio                    |    6244336.00000 |
| Afrikaans                 |    5937881.00000 |
| Ketšua                    |    5768437.00000 |
| Albaniana                 |    5664230.80000 |
| Tatar                     |    5525159.00000 |
| Makua                     |    5471040.00000 |
| Tibetan                   |    5110232.00000 |
| Minangkabau               |    5090568.00000 |
| Kanuri                    |    5043866.00000 |
| Slovak                    |    5024431.50000 |
| Finnish                   |    5016543.10000 |
| Danish                    |    5008464.00000 |
| Tadzhik                   |    4956520.00000 |
| Turkmenian                |    4934965.00000 |
| Wolof                     |    4901011.00000 |
| Ovimbundu                 |    4790616.00000 |
| Batakki                   |    4666354.00000 |
| Bugi                      |    4666354.00000 |
| Creole English            |    4518135.00000 |
| Tswana                    |    4495147.00000 |
| Mandarin Chinese          |    4479132.00000 |
| Malinke                   |    4459198.00000 |
| Norwegian                 |    4386528.00000 |
| Bicol                     |    4330119.00000 |
| Tsonga                    |    4176531.00000 |
| Luhya                     |    4151040.00000 |
| Hebrew                    |    4050068.00000 |
| Armenian                  |    4024652.00000 |
| Zande                     |    3947124.00000 |
| Ganda                     |    3941818.00000 |
| Shan                      |    3876935.00000 |
| Luo                       |    3850240.00000 |
| Banja                     |    3817926.00000 |
| Fang                      |    3794797.00000 |
| Papuan Languages          |    3792451.00000 |
| Hindko                    |    3755592.00000 |
| Mixed Languages           |    3690092.00000 |
| Edo                       |    3679698.00000 |
| Northsotho                |    3674307.00000 |
| Bali                      |    3605819.00000 |
| Gilaki                    |    3588206.00000 |
| Bambara                   |    3572412.00000 |
| Georgiana                 |    3562056.00000 |
| Lomwe                     |    3545240.00000 |
| Ewe                       |    3479156.00000 |
| Dinka                     |    3391350.00000 |
| Kamba                     |    3368960.00000 |
| Kalenjin                  |    3248640.00000 |
| Southsotho                |    3068652.00000 |
| Lithuanian                |    3047066.40000 |
| Ngala and Bangi           |    2995932.00000 |
| Swahili                   |    2949496.00000 |
| Gurage                    |    2940555.00000 |
| Luri                      |    2911186.00000 |
| Waray-waray               |    2886746.00000 |
| Maithili                  |    2847670.00000 |
| Karen                     |    2827882.00000 |
| Bamileke-bamum            |    2805810.00000 |
| Kirgiz                    |    2805303.00000 |
| Mbundu                    |    2781648.00000 |
| Creole French             |    2767307.00000 |
| Bemba                     |    2723193.00000 |
| Tiv                       |    2564638.00000 |
| Hakka                     |    2556672.00000 |
| Dong                      |    2555116.00000 |
| Puyi                      |    2555116.00000 |
| Galecian                  |    2524268.80000 |
| Arabic-French             |    2521118.00000 |
| Ndebele                   |    2517119.00000 |
| Mazandarani               |    2437272.00000 |
| Fon                       |    2426606.00000 |
| Nubian Languages          |    2388690.00000 |
| Sara                      |    2350687.00000 |
| Nkole                     |    2330246.00000 |
| Hehet                     |    2312673.00000 |
| Pampango                  |    2279010.00000 |
| Songhai-zerma             |    2274760.00000 |
| Guaraní                   |    2212225.00000 |
| Hassaniya                 |    2181390.00000 |
| Rakhine                   |    2052495.00000 |
| Bhojpuri                  |    2039088.00000 |
| Ijo                       |    2007108.00000 |
| Sidamo                    |    2002080.00000 |
| Haya                      |    1977503.00000 |
| Makonde                   |    1977503.00000 |
| Rundi                     |    1962852.00000 |
| Tamashek                  |    1936002.00000 |
| Swazi                     |    1915617.00000 |
| Teke                      |    1903797.00000 |
| Slovene                   |    1895003.40000 |
| Beja                      |    1887360.00000 |
| Brahui                    |    1877796.00000 |
| Sena                      |    1849920.00000 |
| Gusii                     |    1834880.00000 |
| Sotho                     |    1830050.00000 |
| Nyakusa                   |    1809918.00000 |
| Kiga                      |    1807574.00000 |
| Soga                      |    1785796.00000 |
| Bura                      |    1784096.00000 |
| Náhuatl                   |    1779858.00000 |
| Kru                       |    1779618.00000 |
| Walaita                   |    1751820.00000 |
| Gur                       |    1729962.00000 |
| Mende                     |    1689192.00000 |
| Meru                      |    1654400.00000 |
| Duala                     |    1644265.00000 |
| Chaga and Pare            |    1642333.00000 |
| Luguru                    |    1642333.00000 |
| Nyanja                    |    1621340.00000 |
| Macedonian                |    1615524.60000 |
| Ga-adangme                |    1576536.00000 |
| Sardinian                 |    1557360.00000 |
| Temne                     |    1543572.00000 |
| Gurma                     |    1504791.00000 |
| Chokwe                    |    1470648.00000 |
| Araucan                   |    1460256.00000 |
| Nuer                      |    1445010.00000 |
| Nyika                     |    1443840.00000 |
| Yao                       |    1442100.00000 |
| Shambala                  |    1441231.00000 |
| Malenasian Languages      |    1438620.00000 |
| Tho                       |    1436976.00000 |
| Pangasinan                |    1367406.00000 |
| Senufo and Minianka       |    1348080.00000 |
| Latvian                   |    1335734.20000 |
| Moravian                  |    1325874.90000 |
| Chuvash                   |    1322406.00000 |
| Gogo                      |    1307163.00000 |
| Teso                      |    1306680.00000 |
| Tharu                     |    1292220.00000 |
| Lango                     |    1284902.00000 |
| Soninke                   |    1271881.00000 |
| Southern Slavic Languages |    1265504.00000 |
| Tigre                     |    1220450.00000 |
| Muong                     |    1197480.00000 |
| Boa                       |    1188042.00000 |
| Serer                     |    1185125.00000 |
| Tswa                      |    1180800.00000 |
| Ha                        |    1173095.00000 |
| Tamang                    |    1172570.00000 |
| Bakhtyari                 |    1150934.00000 |
| Quiché                    |    1149885.00000 |
| [South]Mande              |    1138522.00000 |
| Chuabo                    |    1121760.00000 |
| Tikar                     |    1116290.00000 |
| Tagalog                   |    1113428.00000 |
| Mon                       |    1094664.00000 |
| Yucatec                   |    1087691.00000 |
| Maguindanao               |    1063538.00000 |
| Dzongkha                  |    1062000.00000 |
| Bashkir                   |    1028538.00000 |
| Lugbara                   |    1023566.00000 |
| Cakchiquel                |    1013265.00000 |
| Tonga                     |    1008590.00000 |
| Chin                      |    1003442.00000 |
| Crioulo                   |     996393.00000 |
| Maranao                   |     987571.00000 |
| Gisu                      |     980010.00000 |
| Acholi                    |     958232.00000 |
| Kpelle                    |     956810.00000 |
| Estonian                  |     950140.20000 |
| Romani                    |     943952.00000 |
| Aimará                    |     932809.00000 |
| Mon-khmer                 |     896445.00000 |
| Venda                     |     888294.00000 |
| Newari                    |     885410.00000 |
| Chechen                   |     881604.00000 |
| Mayo-kebbi                |     879865.00000 |
| Nung                      |     878152.00000 |
| Ovambo                    |     875082.00000 |
| Gbaya                     |     860370.00000 |
| Mandara                   |     859845.00000 |
| Banda                     |     849525.00000 |
| Susu                      |     817300.00000 |
| Songhai                   |     775146.00000 |
| Maka                      |     739165.00000 |
| Bari                      |     737250.00000 |
| Mordva                    |     734670.00000 |
| Ngoni                     |     731975.00000 |
| Ronga                     |     728160.00000 |
| Luimbe-nganguela          |     695412.00000 |
| Nyaneka-nkhumbi           |     695412.00000 |
| Friuli                    |     692160.00000 |
| Marendje                  |     688800.00000 |
| Kanem-bornu               |     688590.00000 |
| Adja                      |     676767.00000 |
| Kuy                       |     675389.00000 |
| Ouaddai                   |     665637.00000 |
| Kabyé                     |     638802.00000 |
| Kachin                    |     638554.00000 |
| Basque                    |     631067.20000 |
| Iban                      |     622832.00000 |
| Fur                       |     619290.00000 |
| Diola                     |     594110.00000 |
| Mixtec                    |     593286.00000 |
| Zapotec                   |     593286.00000 |
| Masana                    |     588315.00000 |
| Avarian                   |     587736.00000 |
| Mari                      |     587736.00000 |
| Fries                     |     586968.00000 |
| Lozi                      |     586816.00000 |
| Man                       |     558824.00000 |
| Kekchí                    |     557865.00000 |
| Kymri                     |     536610.60000 |
| Mandjia                   |     535020.00000 |
| Aizo                      |     530439.00000 |
| Bariba                    |     530439.00000 |
| Chewa                     |     522633.00000 |
| Chakma                    |     516620.00000 |
| Hadjarai                  |     512617.00000 |
| Chilluk                   |     501330.00000 |
| Tandjile                  |     497315.00000 |
| Karakalpak                |     486360.00000 |
| Masai                     |     481280.00000 |
| Watyi                     |     476787.00000 |
| Gorane                    |     474362.00000 |
| Luvale                    |     463608.00000 |
| Indian Languages          |     454765.00000 |
| Comorian                  |     453072.00000 |
| Kissi                     |     445800.00000 |
| Lotuko                    |     442350.00000 |
| Udmur                     |     440802.00000 |
| Bassa                     |     432098.00000 |
| Turkana                   |     421120.00000 |
| Busansi                   |     417795.00000 |
| Fijian                    |     415036.00000 |
| Somba                     |     408499.00000 |
| Limba                     |     402882.00000 |
| Otomí                     |     395524.00000 |
| Nsenga                    |     394267.00000 |
| Afar                      |     387574.00000 |
| Yalunka                   |     380506.00000 |
| Dagara                    |     370047.00000 |
| Maltese                   |     364231.60000 |
| Loma                      |     353822.00000 |
| Sranantonga               |     337770.00000 |
| Mboshi                    |     335502.00000 |
| Mbete                     |     310452.00000 |
| Dyula                     |     310362.00000 |
| Ambo                      |     309072.00000 |
| Luchazi                   |     309072.00000 |
| Mam                       |     307395.00000 |
| Arabic-French-English     |     306752.00000 |
| Dhivehi                   |     286000.00000 |
| Lao-Soung                 |     282516.00000 |
| Grebo                     |     280706.00000 |
| Luxembourgish             |     280590.80000 |
| Ngbaka                    |     271125.00000 |
| Tšam                      |     268032.00000 |
| Icelandic                 |     267003.00000 |
| Papiamento                |     266055.00000 |
| Ane                       |     263853.00000 |
| Kotokoli                  |     263853.00000 |
| Marma                     |     258310.00000 |
| Bajan                     |     256770.00000 |
| Moba                      |     249966.00000 |
| Gio                       |     249166.00000 |
| Kono-vai                  |     247554.00000 |
| Dusun                     |     244684.00000 |
| Mbum                      |     231360.00000 |
| Mano                      |     227088.00000 |
| Nama                      |     214024.00000 |
| Punu-sira-nzebi           |     209646.00000 |
| Naudemba                  |     189789.00000 |
| Philippene Languages      |     188156.00000 |
| Assyrian                  |     184920.00000 |
| Bullom-sherbro            |     184452.00000 |
| Kayah                     |     182444.00000 |
| Chibcha                   |     181353.00000 |
| Mpongwe                   |     178996.00000 |
| Lezgian                   |     177882.00000 |
| Balante                   |     177098.00000 |
| Kavango                   |     167422.00000 |
| Maori                     |     166066.00000 |
| Kuranko                   |     165036.00000 |
| Guaymí                    |     151368.00000 |
| Samoan                    |     147108.00000 |
| Hadareb                   |     146300.00000 |
| Tukulor                   |     144180.00000 |
| Gagauzi                   |     140160.00000 |
| Herero                    |     138080.00000 |
| Ami                       |     133536.00000 |
| Garo                      |     129155.00000 |
| Khasi                     |     129155.00000 |
| Santhali                  |     129155.00000 |
| Tripuri                   |     129155.00000 |
| Fukien                    |     128858.00000 |
| Osseetti                  |     119232.00000 |
| Bilin                     |     115500.00000 |
| Saho                      |     115500.00000 |
| Tahitian                  |     109040.00000 |
| Bislama                   |     107540.00000 |
| Garifuna                  |     100693.00000 |
| Tongan                    |      99425.00000 |
| Goajiro                   |      96680.00000 |
| Chiu chau                 |      94948.00000 |
| Malay-English             |      94464.00000 |
| Miskito                   |      94154.00000 |
| Samoan-English            |      93600.00000 |
| San                       |      89564.00000 |
| Atayal                    |      89024.00000 |
| Punu                      |      85347.00000 |
| Kiribati                  |      85135.00000 |
| Abhyasi                   |      84456.00000 |
| Caprivi                   |      81122.00000 |
| Sango                     |      76518.00000 |
| Comorian-French           |      74562.00000 |
| Chamorro                  |      73128.00000 |
| Dorbet                    |      71874.00000 |
| Seselwa                   |      70301.00000 |
| Paiwan                    |      66768.00000 |
| Mahoré                    |      62431.00000 |
| Marshallese               |      61952.00000 |
| Caribbean                 |      61263.00000 |
| Irish                     |      60401.60000 |
| Gaeli                     |      59623.40000 |
| Mandyako                  |      59437.00000 |
| Czech and Moravian        |      59385.70000 |
| Cuna                      |      57120.00000 |
| Arawakan                  |      54375.00000 |
| Circassian                |      50830.00000 |
| Bajad                     |      50578.00000 |
| Trukese                   |      49504.00000 |
| Greenlandic               |      49000.00000 |
| Buryat                    |      45254.00000 |
| Faroese                   |      43000.00000 |
| Romansh                   |      42962.40000 |
| Polynesian Languages      |      41696.00000 |
| Silesiana                 |      41112.40000 |
| Khoekhoe                  |      40550.00000 |
| Bubi                      |      39411.00000 |
| Dariganga                 |      37268.00000 |
| Ukrainian and Russian     |      32392.20000 |
| Zenaga                    |      32040.00000 |
| Comorian-madagassi        |      31790.00000 |
| Eskimo Languages          |      31147.00000 |
| Rapa nui                  |      30422.00000 |
| Pohnpei                   |      28322.00000 |
| Warrau                    |      24170.00000 |
| Maya Languages            |      23136.00000 |
| Embera                    |      17136.00000 |
| Palau                     |      15618.00000 |
| Tuvalu                    |      12535.00000 |
| Sumo                      |      10148.00000 |
| Comorian-Arabic           |       9248.00000 |
| Mortlock                  |       9044.00000 |
| Kosrean                   |       8687.00000 |
| Yap                       |       6902.00000 |
| Nauru                     |       6900.00000 |
| Monegasque                |       5474.00000 |
| Wolea                     |       4403.00000 |
| Carolinian                |       3744.00000 |
| Comorian-Swahili          |       2890.00000 |
| Sinaberberi               |          0.00000 |
| Saame                     |          0.00000 |
| Ainu                      |          0.00000 |
| Niue                      |          0.00000 |
| Pitcairnese               |          0.00000 |
| Nahua                     |          0.00000 |
| Tokelau                   |          0.00000 |
| Futuna                    |          0.00000 |
| Wallis                    |          0.00000 |
| Soqutri                   |          0.00000 |
+---------------------------+------------------+
```
## Question 5 - En quelle unité est exprimée la surface des pays ?
Réponse : la surface des pays est exprimée en km²

## Question 6 - Faire la liste des pays qui ont plus de 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.

```sql
SELECT country.Name, city.Name AS Capital, (city.Population / country.Population * 100) AS PercentageInCapital
FROM country
JOIN city ON country.Capital = city.id
WHERE country.Population > 10000000
ORDER BY country.Population DESC;

---------------------------------------+---------------------+---------------------+
| Name                                  | Capital             | PercentageInCapital |
+---------------------------------------+---------------------+---------------------+
| China                                 | Peking              |              0.5849 |
| India                                 | New Delhi           |              0.0297 |
| United States                         | Washington          |              0.2055 |
| Indonesia                             | Jakarta             |              4.5283 |
| Brazil                                | Brasília            |              1.1580 |
| Pakistan                              | Islamabad           |              0.3352 |
| Russian Federation                    | Moscow              |              5.7095 |
| Bangladesh                            | Dhaka               |              2.7973 |
| Japan                                 | Tokyo               |              6.2978 |
| Nigeria                               | Abuja               |              0.3140 |
| Mexico                                | Ciudad de México    |              8.6885 |
| Germany                               | Berlin              |              4.1218 |
| Vietnam                               | Hanoi               |              1.7662 |
| Philippines                           | Manila              |              2.0813 |
| Egypt                                 | Cairo               |              9.9160 |
| Iran                                  | Teheran             |              9.9832 |
| Turkey                                | Ankara              |              4.5624 |
| Ethiopia                              | Addis Abeba         |              3.9879 |
| Thailand                              | Bangkok             |             10.2936 |
| United Kingdom                        | London              |             12.2184 |
| France                                | Paris               |              3.5884 |
| Italy                                 | Roma                |              4.5832 |
| Congo, The Democratic Republic of the | Kinshasa            |              9.8037 |
| Ukraine                               | Kyiv                |              5.2006 |
| South Korea                           | Seoul               |             21.3082 |
| Myanmar                               | Rangoon (Yangon)    |              7.3704 |
| Colombia                              | Santafé de Bogotá   |             14.7937 |
| South Africa                          | Pretoria            |              1.6312 |
| Spain                                 | Madrid              |              7.2995 |
| Poland                                | Warszawa            |              4.1791 |
| Argentina                             | Buenos Aires        |              8.0529 |
| Tanzania                              | Dodoma              |              0.5639 |
| Algeria                               | Alger               |              6.8889 |
| Canada                                | Ottawa              |              1.0764 |
| Kenya                                 | Nairobi             |              7.6130 |
| Sudan                                 | Khartum             |              3.2129 |
| Morocco                               | Rabat               |              2.1991 |
| Peru                                  | Lima                |             25.1917 |
| Uzbekistan                            | Toskent             |              8.7075 |
| Venezuela                             | Caracas             |              8.1725 |
| North Korea                           | Pyongyang           |             10.3332 |
| Nepal                                 | Kathmandu           |              2.4732 |
| Iraq                                  | Baghdad             |             18.7584 |
| Afghanistan                           | Kabul               |              7.8345 |
| Romania                               | Bucuresti           |              8.9783 |
| Taiwan                                | Taipei              |             11.8679 |
| Malaysia                              | Kuala Lumpur        |              5.8332 |
| Uganda                                | Kampala             |              4.0904 |
| Saudi Arabia                          | Riyadh              |             15.3839 |
| Ghana                                 | Accra               |              5.2939 |
| Mozambique                            | Maputo              |              5.1775 |
| Australia                             | Canberra            |              1.7088 |
| Sri Lanka                             | Colombo             |              3.4259 |
| Yemen                                 | Sanaa               |              2.7805 |
| Kazakstan                             | Astana              |              1.9183 |
| Syria                                 | Damascus            |              8.3535 |
| Madagascar                            | Antananarivo        |              4.2383 |
| Netherlands                           | Amsterdam           |              4.6092 |
| Chile                                 | Santiago de Chile   |             30.9247 |
| Cameroon                              | Yaoundé             |              9.1004 |
| Côte d’Ivoire                         | Yamoussoukro        |              0.8792 |
| Angola                                | Luanda              |             15.7012 |
| Ecuador                               | Quito               |             12.4423 |
| Burkina Faso                          | Ouagadougou         |              6.9029 |
| Zimbabwe                              | Harare              |             12.0833 |
| Guatemala                             | Ciudad de Guatemala |              7.2315 |
| Mali                                  | Bamako              |              7.2063 |
| Cuba                                  | La Habana           |             20.1411 |
| Cambodia                              | Phnom Penh          |              5.1053 |
| Malawi                                | Lilongwe            |              3.9905 |
| Niger                                 | Niamey              |              3.9143 |
| Yugoslavia                            | Beograd             |             11.3158 |
| Greece                                | Athenai             |              7.3212 |
| Czech Republic                        | Praha               |             11.4917 |
| Belgium                               | Bruxelles [Brussel] |              1.3073 |
| Belarus                               | Minsk               |             16.3540 |
| Somalia                               | Mogadishu           |              9.8742 |
| Hungary                               | Budapest            |             18.0376 |
+---------------------------------------+---------------------+---------------------+
```
## Question 7 - Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance

```sql
SELECT Name, (GNP/GNPOld) * 100 AS pourcentage_croissance
FROM country
ORDER BY pourcentage_croissance DESC
LIMIT 10;

+---------------------------------------+------------------------+
| Name                                  | pourcentage_croissance |
+---------------------------------------+------------------------+
| Congo, The Democratic Republic of the |             281.487470 |
| Turkmenistan                          |             219.850000 |
| Tajikistan                            |             188.446970 |
| Estonia                               |             158.053990 |
| Albania                               |             128.200000 |
| Suriname                              |             123.229462 |
| Iran                                  |             122.225899 |
| Bulgaria                              |             119.756122 |
| Honduras                              |             113.540558 |
| Latvia                                |             113.459833 |
+---------------------------------------+------------------------
```

## Question 8 - Liste des pays plurilingues avec pour chacun le nombre de langues parlées.
```sql
SELECT COUNT(language) AS lanques_parlees, country.Name AS Nom_Pays
FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
GROUP BY CountryCode
HAVING COUNT(language)>1
ORDER BY lanques_parlees DESC;

+-----------------+---------------------------------------+
| lanques_parlees | Nom_Pays                              |
+-----------------+---------------------------------------+
|              12 | Canada                                |
|              12 | China                                 |
|              12 | India                                 |
|              12 | Russian Federation                    |
|              12 | United States                         |
|              11 | Tanzania                              |
|              11 | South Africa                          |
|              10 | Congo, The Democratic Republic of the |
|              10 | Iran                                  |
|              10 | Kenya                                 |
|              10 | Mozambique                            |
|              10 | Nigeria                               |
|              10 | Philippines                           |
|              10 | Sudan                                 |
|              10 | Uganda                                |
|               9 | Angola                                |
|               9 | Indonesia                             |
|               9 | Vietnam                               |
|               8 | Australia                             |
|               8 | Austria                               |
|               8 | Cameroon                              |
|               8 | Czech Republic                        |
|               8 | Italy                                 |
|               8 | Liberia                               |
|               8 | Myanmar                               |
|               8 | Namibia                               |
|               8 | Pakistan                              |
|               8 | Sierra Leone                          |
|               8 | Chad                                  |
|               8 | Togo                                  |
|               7 | Benin                                 |
|               7 | Bangladesh                            |
|               7 | Denmark                               |
|               7 | Ethiopia                              |
|               7 | Guinea                                |
|               7 | Kyrgyzstan                            |
|               7 | Nepal                                 |
|               7 | Ukraine                               |
|               6 | Belgium                               |
|               6 | Burkina Faso                          |
|               6 | Central African Republic              |
|               6 | Congo                                 |
|               6 | Germany                               |
|               6 | Eritrea                               |
|               6 | France                                |
|               6 | Micronesia, Federated States of       |
|               6 | Georgia                               |
|               6 | Ghana                                 |
|               6 | Guinea-Bissau                         |
|               6 | Hungary                               |
|               6 | Japan                                 |
|               6 | Kazakstan                             |
|               6 | Latvia                                |
|               6 | Mexico                                |
|               6 | Mali                                  |
|               6 | Mongolia                              |
|               6 | Northern Mariana Islands              |
|               6 | Mauritania                            |
|               6 | Mauritius                             |
|               6 | Malaysia                              |
|               6 | Panama                                |
|               6 | Romania                               |
|               6 | Senegal                               |
|               6 | Sweden                                |
|               6 | Thailand                              |
|               6 | Taiwan                                |
|               6 | Uzbekistan                            |
|               6 | Yugoslavia                            |
|               6 | Zambia                                |
|               5 | Afghanistan                           |
|               5 | Brazil                                |
|               5 | Botswana                              |
|               5 | Côte d’Ivoire                         |
|               5 | Colombia                              |
|               5 | Comoros                               |
|               5 | Estonia                               |
|               5 | Finland                               |
|               5 | Gambia                                |
|               5 | Guatemala                             |
|               5 | Guam                                  |
|               5 | Hong Kong                             |
|               5 | Iraq                                  |
|               5 | Lithuania                             |
|               5 | Luxembourg                            |
|               5 | Moldova                               |
|               5 | Macedonia                             |
|               5 | Niger                                 |
|               5 | Norway                                |
|               5 | Nauru                                 |
|               5 | Réunion                               |
|               5 | Slovakia                              |
|               4 | Aruba                                 |
|               4 | Andorra                               |
|               4 | Azerbaijan                            |
|               4 | Bulgaria                              |
|               4 | Belarus                               |
|               4 | Belize                                |
|               4 | Bolivia                               |
|               4 | Brunei                                |
|               4 | Switzerland                           |
|               4 | Chile                                 |
|               4 | Costa Rica                            |
|               4 | Spain                                 |
|               4 | Gabon                                 |
|               4 | Honduras                              |
|               4 | Cambodia                              |
|               4 | Laos                                  |
|               4 | Macao                                 |
|               4 | Monaco                                |
|               4 | Malawi                                |
|               4 | Nicaragua                             |
|               4 | Netherlands                           |
|               4 | Palau                                 |
|               4 | Poland                                |
|               4 | Paraguay                              |
|               4 | Turkmenistan                          |
|               4 | Zimbabwe                              |
|               3 | Albania                               |
|               3 | Netherlands Antilles                  |
|               3 | Argentina                             |
|               3 | American Samoa                        |
|               3 | Burundi                               |
|               3 | Bhutan                                |
|               3 | Djibouti                              |
|               3 | United Kingdom                        |
|               3 | Guyana                                |
|               3 | Israel                                |
|               3 | Jordan                                |
|               3 | Lebanon                               |
|               3 | Liechtenstein                         |
|               3 | Sri Lanka                             |
|               3 | Lesotho                               |
|               3 | Mayotte                               |
|               3 | New Caledonia                         |
|               3 | Peru                                  |
|               3 | French Polynesia                      |
|               3 | Singapore                             |
|               3 | Solomon Islands                       |
|               3 | Slovenia                              |
|               3 | Seychelles                            |
|               3 | Tajikistan                            |
|               3 | Trinidad and Tobago                   |
|               3 | Tunisia                               |
|               3 | Turkey                                |
|               3 | Tuvalu                                |
|               3 | Venezuela                             |
|               3 | Virgin Islands, U.S.                  |
|               3 | Vanuatu                               |
|               3 | Samoa                                 |
|               2 | United Arab Emirates                  |
|               2 | Armenia                               |
|               2 | Antigua and Barbuda                   |
|               2 | Bahrain                               |
|               2 | Bahamas                               |
|               2 | Barbados                              |
|               2 | Cocos (Keeling) Islands               |
|               2 | Cook Islands                          |
|               2 | Cape Verde                            |
|               2 | Christmas Island                      |
|               2 | Cyprus                                |
|               2 | Dominica                              |
|               2 | Dominican Republic                    |
|               2 | Algeria                               |
|               2 | Ecuador                               |
|               2 | Egypt                                 |
|               2 | Fiji Islands                          |
|               2 | Faroe Islands                         |
|               2 | Gibraltar                             |
|               2 | Guadeloupe                            |
|               2 | Equatorial Guinea                     |
|               2 | Greece                                |
|               2 | Greenland                             |
|               2 | French Guiana                         |
|               2 | Croatia                               |
|               2 | Haiti                                 |
|               2 | Ireland                               |
|               2 | Iceland                               |
|               2 | Jamaica                               |
|               2 | Kiribati                              |
|               2 | Saint Kitts and Nevis                 |
|               2 | South Korea                           |
|               2 | Kuwait                                |
|               2 | Libyan Arab Jamahiriya                |
|               2 | Saint Lucia                           |
|               2 | Morocco                               |
|               2 | Madagascar                            |
|               2 | Maldives                              |
|               2 | Marshall Islands                      |
|               2 | Malta                                 |
|               2 | Martinique                            |
|               2 | Niue                                  |
|               2 | New Zealand                           |
|               2 | Oman                                  |
|               2 | Papua New Guinea                      |
|               2 | Puerto Rico                           |
|               2 | North Korea                           |
|               2 | Palestine                             |
|               2 | Qatar                                 |
|               2 | Rwanda                                |
|               2 | Svalbard and Jan Mayen                |
|               2 | El Salvador                           |
|               2 | Somalia                               |
|               2 | Sao Tome and Principe                 |
|               2 | Suriname                              |
|               2 | Swaziland                             |
|               2 | Syria                                 |
|               2 | Tokelau                               |
|               2 | East Timor                            |
|               2 | Tonga                                 |
|               2 | Saint Vincent and the Grenadines      |
|               2 | Wallis and Futuna                     |
|               2 | Yemen                                 |
+-----------------+---------------------------------------+
212 rows in set (0,00 sec)
```
## Question 9 - Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.

```sql
SELECT country.Name AS Nom_Pays,COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END) AS langues_off, COUNT(language) AS lanques_parlees
FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
GROUP BY CountryCode
HAVING COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END)>1
ORDER BY langues_off DESC;

+----------------------+-------------+-----------------+
| Nom_Pays             | langues_off | lanques_parlees |
+----------------------+-------------+-----------------+
| South Africa         |           4 |              11 |
| Switzerland          |           4 |               4 |
| Luxembourg           |           3 |               5 |
| Vanuatu              |           3 |               3 |
| Singapore            |           3 |               3 |
| Peru                 |           3 |               3 |
| Bolivia              |           3 |               4 |
| Belgium              |           3 |               6 |
| Somalia              |           2 |               2 |
| Nauru                |           2 |               5 |
| Palau                |           2 |               4 |
| Paraguay             |           2 |               4 |
| Romania              |           2 |               6 |
| Rwanda               |           2 |               2 |
| Burundi              |           2 |               3 |
| Malta                |           2 |               2 |
| Seychelles           |           2 |               3 |
| Togo                 |           2 |               8 |
| Tonga                |           2 |               2 |
| Tuvalu               |           2 |               3 |
| American Samoa       |           2 |               3 |
| Samoa                |           2 |               3 |
| Netherlands Antilles |           2 |               3 |
| Marshall Islands     |           2 |               2 |
| Madagascar           |           2 |               2 |
| Afghanistan          |           2 |               5 |
| Lesotho              |           2 |               3 |
| Sri Lanka            |           2 |               3 |
| Kyrgyzstan           |           2 |               7 |
| Israel               |           2 |               3 |
| Ireland              |           2 |               2 |
| Guam                 |           2 |               5 |
| Greenland            |           2 |               2 |
| Faroe Islands        |           2 |               2 |
| Finland              |           2 |               5 |
| Cyprus               |           2 |               2 |
| Belarus              |           2 |               4 |
| Canada               |           2 |              12 |
+----------------------+-------------+-----------------+
38 rows in set (0,01 sec)
```

## Question 10 -Liste des langues parlées en France avec le % pour chacune.
```sql
SELECT  country.Name AS Nom_Pays,language AS lanques_parlees, Percentage
FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
HAVING country.Name ='France' 
ORDER BY Percentage DESC;

+----------+-----------------+------------+
| Nom_Pays | lanques_parlees | Percentage |
+----------+-----------------+------------+
| France   | French          |       93.6 |
| France   | Arabic          |        2.5 |
| France   | Portuguese      |        1.2 |
| France   | Italian         |        0.4 |
| France   | Spanish         |        0.4 |
| France   | Turkish         |        0.4 |
+----------+-----------------+------------+
6 rows in set (0,00 sec)
```

## Question 11 -Liste des langues parlées en Chine avec le % pour chacune.
```sql
SELECT  country.Name AS Nom_Pays,language AS lanques_parlees, Percentage
FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
HAVING country.Name ='China' 
ORDER BY Percentage DESC;

+----------+-----------------+------------+
| Nom_Pays | lanques_parlees | Percentage |
+----------+-----------------+------------+
| China    | Chinese         |       92.0 |
| China    | Zhuang          |        1.4 |
| China    | Mantšu          |        0.9 |
| China    | Hui             |        0.8 |
| China    | Miao            |        0.7 |
| China    | Uighur          |        0.6 |
| China    | Yi              |        0.6 |
| China    | Tujia           |        0.5 |
| China    | Mongolian       |        0.4 |
| China    | Tibetan         |        0.4 |
| China    | Dong            |        0.2 |
| China    | Puyi            |        0.2 |
+----------+-----------------+------------+
12 rows in set (0,01 sec)
```
## Question 12 -Liste des langues parlées aux Etats Unis avec le % pour chacune.
```sql
SELECT  country.Name AS Nom_Pays,language AS lanques_parlees, Percentage
FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
HAVING country.Name ='United States' 
ORDER BY Percentage DESC;

+---------------+-----------------+------------+
| Nom_Pays      | lanques_parlees | Percentage |
+---------------+-----------------+------------+
| United States | English         |       86.2 |
| United States | Spanish         |        7.5 |
| United States | French          |        0.7 |
| United States | German          |        0.7 |
| United States | Chinese         |        0.6 |
| United States | Italian         |        0.6 |
| United States | Tagalog         |        0.4 |
| United States | Korean          |        0.3 |
| United States | Polish          |        0.3 |
| United States | Japanese        |        0.2 |
| United States | Portuguese      |        0.2 |
| United States | Vietnamese      |        0.2 |
+---------------+-----------------+------------+
12 rows in set (0,00 sec)
```
## Question 13 -Liste des langues parlées au UK avec le % pour chacune.
```sql
SELECT  country.Name AS Nom_Pays,language AS lanques_parlees, Percentage
FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
HAVING country.Name ='United Kingdom' 
ORDER BY Percentage DESC;

| Nom_Pays       | lanques_parlees | Percentage |
+----------------+-----------------+------------+
| United Kingdom | English         |       97.3 |
| United Kingdom | Kymri           |        0.9 |
| United Kingdom | Gaeli           |        0.1 |
+----------------+-----------------+------------+
3 rows in set (0,00 sec)
```
## Question 14 - Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle. <- on la fera en python demain


## Qustion 15 - Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi ?


## Question 16 - Faire une carte du monde, avec une couleur par région. (chaque pays étant dans la bonne couleur).

![world_map](worldmap.png)



































